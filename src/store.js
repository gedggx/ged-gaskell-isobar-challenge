import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    gameScore: 0
  },
  mutations: {
    incrementScore: state => {
      state.gameScore++;
    },
    resetScore: state => {
      state.gameScore = 0;
    }
  },
  actions: {}
});
